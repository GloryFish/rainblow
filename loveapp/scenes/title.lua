--
--  base.lua
--  rogue-descent
--
--  A base scene that can be used when creating new scenes.
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'menu'

local scene = Gamestate.new()

function scene:enter(pre)
  self.music = love.audio.newSource('resources/music/dark-ambiance.mp3', 'stream')
  self.music:setLooping('true')
  love.audio.play(self.music)

  self.musichappy = love.audio.newSource('resources/music/fluffy-style.mp3', 'stream')
  self.musichappy:setLooping('true')
  self.musichappy:setVolume(0)
  love.audio.play(self.musichappy)

  self.title = {
    sprite = love.graphics.newImage('resources/sprites/title.png'),
  }
  self.title.position = vector((love.graphics.getWidth() / 2) - self.title.sprite:getWidth() / 2, 150)

  self.lovelogo = {
    sprite = love.graphics.newImage('resources/sprites/lovelogo.png'),
  }
  self.lovelogo.position = vector(love.graphics.getWidth() - 150, love.graphics.getHeight() - 100)

  self.camera = Camera()
  self.camera.position = vector(608, 700)
  self.camera.smoothMovement = true
  self.camera.focus = self.camera.position
  self.camera.handheld = true
  self.camera.scale = 1
  self.camera.bounds = {
    top = -2000,
    left = -2000,
    bottom = 2000,
    right = 2000,
  }

  self.menu = Menu(vector((love.graphics.getWidth() / 2) - 150, (love.graphics.getHeight() / 2 - 30)))
  local playButton = TextButton('Play')
  playButton.action = function()
  self:startGame()
  end
  self.menu:addButton(playButton)

  local quitButton = TextButton('Quit')
  quitButton.action = function()
    self:quit()
  end
  self.menu:addButton(quitButton)

  self.world = World('resources/maps/worldmap.tmx')
  self.world.camera = self.camera

  Shaders.currentEffect = 'distort'
end

function scene:keypressed(key, unicode)
  if key == 'escape' then
    self:quit()
  end

  if key == ' ' then
    self:startGame()
  end

  if key == 'f' then
    System:toggleFullscreen()
  end
end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
  self.menu:mousereleased(vector(x, y))
end

function scene:update(dt)
  Timer.update(dt)
  Tween.update(dt)
  self.menu:update(dt)
  self.camera:update(dt)
end

function scene:draw()
  love.graphics.clear()
  colors.white:set()

  love.graphics.setCanvas(Canvases.buffer)
  Canvases.buffer:clear(colors.green:unpack())
  love.graphics.setPixelEffect()

  Sprites.main.batch:clear()

  self.camera:apply()

  self.world:draw()

  colors.white:set()
  love.graphics.draw(Sprites.main.batch)

  self.camera:unapply()

  colors.white:set()
  loveframes.draw()

  -- Title UI
  local scale = math.sin(love.timer.getTime())

  love.graphics.draw(self.title.sprite, math.floor(self.title.position.x), math.floor(self.title.position.y) + scale * 5)

  love.graphics.setFont(fonts.default)

  colors.black:set()
  love.graphics.print('Created by Jay Roberts', self.lovelogo.position.x - 410 + 1, self.lovelogo.position.y + 20 + 1)
  love.graphics.print('Made with', self.lovelogo.position.x - 120 + 1, self.lovelogo.position.y + 20 + 1)


  colors.white:set()
  love.graphics.print('Created by Jay Roberts', self.lovelogo.position.x - 410, self.lovelogo.position.y + 20)
  love.graphics.print('Made with', self.lovelogo.position.x - 120, self.lovelogo.position.y + 20)

  love.graphics.draw(self.lovelogo.sprite, self.lovelogo.position.x, self.lovelogo.position.y, 0, 0.25, 0.25)

  self.menu:draw()

  Sprites.ui.batch:clear()
  Cursor:draw()
  love.graphics.draw(Sprites.ui.batch)

  love.graphics.setCanvas()
  Shaders:set(Shaders.currentEffect)

  love.graphics.drawq(Canvases.buffer, Shaders.quad, 0, 0)
  love.graphics.setPixelEffect()

  Console:draw()

  love.graphics.print(string.format('Title'), love.graphics.getWidth() - 430, 10)

end

function scene:startGame()
  local fadeOutTime = 0

  local startTime = love.timer.getTime()
  Timer.do_for(fadeOutTime, function(dt)
    local progress = (love.timer.getTime() - startTime) / fadeOutTime
    self.music:setVolume(1 - progress)
  end,
  function()
    scenes.game.camera = self.camera
    scenes.game.world = self.world
    scenes.game.musichappy = self.musichappy
    scenes.game.musicdark = self.music
    Gamestate.switch(scenes.game)
  end)
end


function scene:quit()
  love.event.push('quit')
end

function scene:leave()
end

return scene