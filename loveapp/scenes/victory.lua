--
--  base.lua
--  rogue-descent
--
--  A base scene that can be used when creating new scenes.
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'menu'

local scene = Gamestate.new()

function scene:enter(pre)
  self.title = {
    sprite = love.graphics.newImage('resources/sprites/win.png'),
  }
  self.title.position = vector((love.graphics.getWidth() / 2) - self.title.sprite:getWidth() / 2, -100)

  Tween.start(3, self.title.position, { y = 150 }, 'outElastic')

  self.menu = Menu(vector((love.graphics.getWidth() / 2), (love.graphics.getHeight() / 2 - 30)))
  local playButton = TextButton('Play Again')
  playButton.action = function()
  self:startGame()
  end
  self.menu:addButton(playButton)

  local quitButton = TextButton('Quit')
  quitButton.action = function()
    self:quit()
  end
  self.menu:addButton(quitButton)

  self.menu.visible = false
  Timer.add(3, function()
    self.menu.visible = true
  end)

end

function scene:keypressed(key, unicode)
  if key == 'escape' then
    self:quit()
  end

  if key == ' ' then
    self:startGame()
  end

  if key == 'f' then
    System:toggleFullscreen()
  end
end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
  self.menu:mousereleased(vector(x, y))
end

function scene:update(dt)
  Timer.update(dt)
  Tween.update(dt)
  self.menu:update(dt)
  self.camera:update(dt)
end

function scene:draw()
  love.graphics.clear()
  colors.white:set()

  love.graphics.setCanvas(Canvases.buffer)
  Canvases.buffer:clear(colors.green:unpack())
  love.graphics.setPixelEffect()

  Sprites.main.batch:clear()

  self.camera:apply()

  self.world:draw()

  colors.white:set()
  love.graphics.draw(Sprites.main.batch)

  self.camera:unapply()

  colors.white:set()
  loveframes.draw()

  -- Title UI

  love.graphics.draw(self.title.sprite, math.floor(self.title.position.x), math.floor(self.title.position.y))

  love.graphics.setFont(fonts.default)

  self.menu:draw()

  Sprites.ui.batch:clear()
  Cursor:draw()
  love.graphics.draw(Sprites.ui.batch)

  love.graphics.setCanvas()
  Shaders:set(Shaders.currentEffect)

  love.graphics.drawq(Canvases.buffer, Shaders.quad, 0, 0)
  love.graphics.setPixelEffect()

  Console:draw()

  love.graphics.print(string.format('Title'), love.graphics.getWidth() - 430, 10)

end

function scene:startGame()
  local fadeOutTime = 0

  local startTime = love.timer.getTime()
  Timer.do_for(fadeOutTime, function(dt)
    local progress = (love.timer.getTime() - startTime) / fadeOutTime
    self.music:setVolume(1 - progress)
  end,
  function()
    Gamestate.switch(scenes.game)
  end)
end


function scene:quit()
  love.event.push('quit')
end

function scene:leave()
end

return scene