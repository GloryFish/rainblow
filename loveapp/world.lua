require 'middleclass'
require 'astar'
require 'vector'
require 'spatialhash'
require 'rectangle'

local HC = require 'hardoncollider'

World = class('World')


function World:initialize(filename)
  print(filename)
  self.tilemap = ATL.Loader.load(filename)

  self.spatialhash = SpatialHash(100)
  self.paths = {}
  self:loadPaths()

  self.spawns = {}
  self:loadSpawns()
end

function World:speedFactorAtPosition(position)
  local tileCoords = vector(math.floor(position.x / self.tilemap.tilewidth), math.floor(position.y / self.tilemap.tileheight))
  local tile = self.tilemap.layers['slow']:get(tileCoords.x, tileCoords.y)

  if tile and tile.properties['slow'] then
    return 0.3
  else
    return 1
  end
end

function World:loadPaths()
  local objects = self.tilemap.layers['paths'].objects

  for index, object in ipairs(objects) do
    local path = {}
    for i = 1, #object.polyline - 1, 2 do
      table.insert(path, vector(object.polyline[i] + object.x, object.polyline[i + 1] + object.y))
    end
    table.insert(self.paths, path)
  end
end

function World:loadSpawns()
  for name, layer in pairs(self.tilemap.layers) do
    print(name)
  end
  local objects = self.tilemap.layers['spawns'].objects

  for index, object in ipairs(objects) do
    table.insert(self.spawns, vector(object.x, object.y))
  end
end

function World:randomSpawn()
  return self.spawns[math.random(#self.spawns)]
end

function World:getBounds()
  return Rectangle(vector(0, 0), vector(self.tilemap.width * self.tilemap.tilewidth, self.tilemap.height * self.tilemap.tileheight))
end

function World:pointIsWalkable(point)
  -- TODO?
  return true
end

function World:draw()
  self.tilemap:draw(0, 0)

end