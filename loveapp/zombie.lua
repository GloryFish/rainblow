require 'middleclass'
require 'vector'
require 'boid'
require 'bar'

Zombie = class('Zombie', Boid)

local deathSounds = {}
for i = 1, 18 do
  table.insert(deathSounds, string.format('death-%d.mp3', i))
end

local zombieSounds = {}
for i = 1, 24 do
  table.insert(zombieSounds, string.format('zombie-%d.mp3', i))
end

function Zombie:initialize(world)
  Boid.initialize(self)

  -- Spritesheet
  self.spritesheet = Sprites.zombies

  -- Size and position
  self.position = vector(0, 0)
  self.rotation = 0
  self.scale = 0.5
  self.flip = -1
  self.offset = vector(24, 16)
  self.direction = vector(0, -1)

  self.zombieType = math.random(1, 6)
  self.bossSpeedFactor = 1
  self.isBoss = false

  self.healthCurrent = 1
  self.healthMax = 1

  self.dead = false

  self.world = world

  -- Animation
  self.currentFrameIndex = 1
  self.animationName = string.format('zombie_%d_down', self.zombieType)
  self.animationElapsed = 0
  print(self.animationName)

  Notifier:listenForMessage('rainbow_blast', self)
  Notifier:listenForMessage('rainbow_calm', self)
end

function Zombie:setBoss()
  self.zombieType = 7
  self.healthCurrent = 5000
  self.healthMax = 5000
  self.offset = vector(32, 90)
  self.bossSpeedFactor = 0.7
  self.bar = Bar(self, colors.red)
  self.size = self.size * 2
  self.isBoss = true
end

function Zombie:receiveMessage(message, data)
  if message == 'rainbow_blast' then
    local blast = data

    local dist = blast.position:dist(self.position)

    if dist - self.size - blast.radius < 0 then
      self:takeDamage(blast.damage)
    elseif dist < 150 then
      self:setCohesionFactor(-2)
    else
      self:setCohesionFactor(1)
    end

  elseif message == 'rainbow_calm' then
    self:setCohesionFactor(1)
  end
end

function Zombie:setBloodEmitter(emitter)
  self.bloodEmitter = emitter
end

function Zombie:takeDamage(amount)
  self.healthCurrent = self.healthCurrent - amount

  if not self.dead then
    self.bloodEmitter:addGib(self.position)
  end

  if self.bar then
    self.bar.amount = math.max(self.healthCurrent / self.healthMax, 0)
  end

  if self.healthCurrent < 0 then
    self.healthCurrent = 0
    self:die()
  end
end

function Zombie:die()
  if self.dead then
    return
  end
  self.dead = true
  Audio:playRandomSound(deathSounds)
  Notifier:postMessage('zombie_died', self)
  if self.isBoss then
    Notifier:postMessage('boss_zombie_died', self)
  end
end

function Zombie:getPosition()
  return self.position
end

function Zombie:setPosition(position)
  self.position = position
end

function Zombie:setAnimation(name)
  if (self.animationName ~= name) then
    self.animationName = name
    self.currentFrameIndex = 1
    self.animationElapsed = 0
  end
end

-- Returns a vector representing the current size, based on the active quad
function Zombie:getCurrentSize()
  local animation = self.spritesheet.animations[self.animationName]
  local currentFrame = animation.frames[self.currentFrameIndex]
  local quad = self.spritesheet.quads[currentFrame.name]
  local x, y, w, h = quad:getViewport()
  return vector(w, h)
end

function Zombie:update(dt, zombies)
  self.speedFactor = self.world:speedFactorAtPosition(self.position) * self.bossSpeedFactor

  Boid.update(self, dt, zombies)

  local direction = self.velocity:movementDirection()

  self:setAnimation(string.format('zombie_%d_%s', self.zombieType, direction))

  if math.random() < 0.002 then
    Audio:playRandomSound(zombieSounds, 'moans')
  end

  if self.bar then
    self.bar:update(dt)
  end

  -- Handle animation
  self.animationElapsed = self.animationElapsed + dt
  local animation = self.spritesheet.animations[self.animationName]
  if #animation.frames > 1 then -- More than one frame
    local duration = animation.frames[self.currentFrameIndex].duration

    if self.animationElapsed > duration then -- Switch to next frame
      self.currentFrameIndex = self.currentFrameIndex + 1
      if self.currentFrameIndex > #animation.frames then -- Aaaand back around
        self.currentFrameIndex = 1
      end
      self.animationElapsed = self.animationElapsed - duration
    end
  end
end

function Zombie:draw()
  local animation = self.spritesheet.animations[self.animationName]
  local currentFrame = animation.frames[self.currentFrameIndex]
  self.spritesheet.batch:addq(self.spritesheet.quads[currentFrame.name],
                              math.floor(self.position.x),
                              math.floor(self.position.y),
                              animation.rotation - self.rotation,
                              self.scale * self.flip,
                              self.scale,
                              self.offset.x,
                              self.offset.y)

  if self.bar then
    self.bar:draw()
  end
end