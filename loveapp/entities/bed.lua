-- Apple

require 'middleclass'
require 'actions/actions'

local EntityBed = class('EntityBed', Entity)


function EntityBed:initialize()
  Entity.initialize(self)

  self.animationName = 'entity_bed_idle'
end

function EntityBed:getAdvertisements()
  local ads = {
    {
      name = 'sleep',
      rewards = {
        fatigue = 50,
      },
      actions = {
        ActionMoveTo(self.position),
        ActionWait(5, 'Sleeping'),
        ActionCustom('Sleep',
          function(this, dt) -- update
            this.agent:changeNeed('fatigue', 50)
          end,
          function() -- done
            return true
          end
         ),
      }
    },
  }

  return ads
end

function EntityBed:draw()
  Entity.draw(self)
end

return EntityBed