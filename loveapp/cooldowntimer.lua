require 'middleclass'
require 'vector'


CooldownTimer = class('CooldownTimer')

function CooldownTimer:initialize(rainbow)
  self.rainbow = rainbow
  self.sad = love.graphics.newImage('resources/sprites/cloud_sad.png')
  self.happy = love.graphics.newImage('resources/sprites/cloud_happy.png')
  self.highlight = love.graphics.newImage('resources/sprites/cloud_highlight.png')
  self.position = vector(90, love.graphics.getHeight() - 90)
  self.offset = vector(128, 128)
  self.bounce = 0
  self.scale = 1
end

function CooldownTimer:update(dt)
  self.bounce = math.sin(love.timer.getTime())
end

function CooldownTimer:draw()
  if self.rainbow.firing then
    return
  end
  colors.white:set()
  love.graphics.draw(self.sad, self.position.x, self.position.y + self.bounce * 8, 0, self.scale, self.scale, self.offset.x, self.offset.y)

  local progress = self.rainbow:cooldownProgress()


  love.graphics.setScissor(self.position.x - 128, self.position.y - 128 + 35 + 185 - progress * 185, 256, 512)

  -- colors.white:alpha(255 * progress):set()
  love.graphics.draw(self.happy, self.position.x, self.position.y + self.bounce * 8, 0, self.scale, self.scale, self.offset.x, self.offset.y)

  love.graphics.setScissor()

  if progress == 1 then
    love.graphics.draw(self.highlight, self.position.x, self.position.y + self.bounce * 8, 0, self.scale, self.scale, self.offset.x, self.offset.y)
  end
end