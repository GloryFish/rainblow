require 'middleclass'
require 'vector'

Blood = class('Blood')

local kinds = {
  'blood',
  'splatter',
}

local random_unit_vector = function()
  local vec = vector(0, -1)
  local rotation = math.random() * math.pi
  return vec:rotate_inplace(rotation)
end

function Blood:initialize()
  self.gibImages = {}
  self.gibImages['blood'] = love.graphics.newImage('resources/sprites/blood.png')
  self.gibImages['splatter'] = love.graphics.newImage('resources/sprites/blood_splatter.png')

  self.maxgibs = 500

  self.gibs = {}
end

function Blood:addGib(pos)
  table.insert(self.gibs, {
    position = pos + random_unit_vector() * math.random(20),
    kind = kinds[math.random(#kinds)],
    alpha = 255,
    rotation = math.random() * math.pi,
    scale = math.random(500, 800) / 1000
  })

  if #self.gibs > self.maxgibs then
    Tween.start(5, self.gibs[1], { alpha = 0 }, 'linear', function()
      table.remove(self.gibs, 1)
    end)
  end
end

function Blood:update(dt)
end

function Blood:draw()
  colors.white:alpha(200):set()
  for index, gib in ipairs(self.gibs) do
    love.graphics.draw(self.gibImages[gib.kind], gib.position.x, gib.position.y, gib.rotation, gib.scale, gib.scale)
  end
end