require 'middleclass'
require 'vector'
require 'bar'

Scoreboard = class('Scoreboard')

function Scoreboard:initialize()
  self.score = 0
  self.font = love.graphics.newFont('resources/fonts/Herculanum.ttf', 24)
  self.position = vector(love.graphics.getWidth() - 270, love.graphics.getHeight() - 70)
  self.color = colors.red

  self.bar = Bar(self, colors.red)
  self.bar.width = 200
  self.bar.amount = 0
  self.bar.offset = vector(100, 0)
  self.window = 10
  self.kills = {}

  self.killsReached = false

  self.neededKills = 10

  Notifier:listenForMessage('zombie_died', self)
end

function Scoreboard:receiveMessage(message, data)
  if message == 'zombie_died' then
    local deadZombie = data
    table.insert(self.kills, love.timer.getTime())
  end
end

function Scoreboard:update(dt)
  for i = #self.kills, 1, -1 do
    if self.kills[i] < love.timer.getTime() - self.window then
      table.remove(self.kills, i)
    end
  end

  self.score = #self.kills
  if self.score >= self.neededKills then
    if not self.killsReached then
      Notifier:postMessage('needed_kills_reached')
    end
    self.killsReached = true

    self.score = self.neededKills
  end

  self.bar.amount = self.score / self.neededKills
  self.bar:update(dt)
end

function Scoreboard:draw()
  love.graphics.setFont(self.font)

  if self.killsReached then
    colors.black:set()
    love.graphics.print(string.format('KILL THE BOSS ZOMBIE!', #self.kills, self.window), self.position.x + 1, self.position.y + 1)

    self.color:set()
    love.graphics.print(string.format('KILL THE BOSS ZOMBIE!', #self.kills, self.window), self.position.x, self.position.y)
  else
    self.bar:draw()
  end
end
