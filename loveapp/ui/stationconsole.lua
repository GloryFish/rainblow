--
--  Stationconsole.lua
--  rogue-descent
--
--  Created by Jay Roberts on 2012-04-27.
--  Copyright 2012 GloryFish.org. All rights reserved.
--

require 'middleclass'
require 'vector'
require 'rectangle'
require 'colors'
require 'notifier'

StationConsole = class('StationConsole')

function StationConsole:initialize()
  self.position = vector(0, 0)
  self.font = love.graphics.newFont('resources/fonts/NotCourierSans.ttf', 16)
  self.lines = {}
  self.maxLines = 100
  Notifier:listenForMessage('station_console_log', self)

  -- Parse startup text
  self.startupLines = {}
  for line in love.filesystem.lines('resources/data/console-startup.txt') do
    table.insert(self.startupLines, line)
  end

end

function StationConsole:setPosition(position)
  self.position = position
end

function StationConsole:startup()
  self.startTime = love.timer.getTime()
  self.lines = {}
  for index, line in ipairs(self.startupLines) do
    self:addLine(line)
  end
end

function StationConsole:getTime()
  local now = love.timer.getTime()
  local elapsed = now - self.startTime

  local hours =  elapsed / 3600
  local minutes =  (elapsed / 60) % 60
  local seconds =  elapsed % 60

  return string.format('%02d:%02d:%02d', hours, minutes, seconds)
end

function StationConsole:addLine(line)
  local time = self:getTime()
  local entry = string.format('%s: %s', time, line)

  table.insert(self.lines, entry)

  if #self.lines > self.maxLines then
    table.remove(self.lines, 1)
  end
end

function StationConsole:receiveMessage(message, data)
  print(message)
  if message == 'station_console_log' then
    local line = data
    self:addLine(line)
  end
end

function StationConsole:draw()
  love.graphics.setFont(self.font)
  colors.black:set()

  local lineHeight = self.font:getHeight() + 2
  local lineOffset = self.position.y - lineHeight * #self.lines

  for i, line in ipairs(self.lines) do
    local textPosition = vector(self.position.x, lineOffset + lineHeight * (i - 1))

    love.graphics.print(line,
                        textPosition.x,
                        textPosition.y)
  end
end
