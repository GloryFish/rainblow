--
--  game.lua
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'agent'
require 'entity'
require 'camera'
require 'background'
require 'station'
require 'gesturerecognizer'
require 'ui/agentinfoframe'
require 'ui/stationinfoframe'
require 'ui/missiontimelabel'
require 'ui/editortoolbar'
require 'ui/menubar'
require 'ui/stationconsole'

local scene = Gamestate.new()

function scene:enter(pre)
  Console.delegate = self

  self.gesturerecognizer = GestureRecognizer()

  self.drawables = {}
  self.updatables ={}

  self.camera = Camera()
  self.camera.position = vector(512, 150)
  self.camera.focus = vector(512, 150)
  self.camera.handheld = false
  self.camera.scale = 1
  self.camera.bounds = {
    top = -2000,
    left = -2000,
    bottom = 2000,
    right = 2000,
  }

  self.stationConsole = StationConsole()
  self.stationConsole:setPosition(vector(30, love.graphics.getHeight() - 70))
  self.stationConsole:startup()

  self.background = Background()
  table.insert(self.drawables, self.background)

  self.station = Station()
  table.insert(self.drawables, self.station)

  self.agent = self.station:spawnAgent()

  Notifier:listenForMessage('mouse_down', self)
  Notifier:listenForMessage('agent_selected', self)
  Notifier:listenForMessage('agent_spawned', self)
  Notifier:listenForMessage('set_view_mode', self)

  -- Tick
  self.tickPeriod = 3
  self.tickDuration = 0

  -- UI components
  self.viewMode = 'console'

  self.menuBar = MenuBar()
  self.menuBar:setStation(self.station)
  self.menuBar:show()

  self.agentInfoFrame = AgentInfoFrame()
  self.missionTimeLabel = MissionTimeLabel()
  self.missionTimeLabel:start()

  self.agentInfoFrame:showForAgent(self.agent)

  self.stationInfoFrame = StationInfoFrame()
  self.stationInfoFrame:showForStation(self.station)

  self.paused = false
  self.timescale = 1

  Shaders.currentEffect = 'distort'
end

function scene:keypressed(key, unicode)
  if Console:keypressed(key, unicode) then
    return
  end

  loveframes.keypressed(key, unicode)

  if key == 'escape' then
    self:quit()
  end

  if key == 'p' then
    self.paused = not self.paused
  end

  if key == ']' then
    self.timescale = self.timescale + 0.5
  end

  if key == '[' then
    self.timescale = self.timescale - 0.5
  end

  if key == '`' then
    Console:open()
  end

  if key == 'f' then
    love.graphics.toggleFullscreen()
  end
end

function scene:keyreleased(key, unicode)
  loveframes.keypressed(key, unicode)
end

function scene:mousepressed(x, y, button)
  loveframes.mousepressed(x, y, button)

  if button == 'wu' then
    self.camera.scale = self.camera.scale + self.camera.scale * 0.1
    print(self.camera.scale)
  elseif button == 'wd' then
    self.camera.scale = self.camera.scale - self.camera.scale * 0.1
    print(self.camera.scale)
  end
end

function scene:mousereleased(x, y, button)
  loveframes.mousereleased(x, y, button)
end

function scene:receiveMessage(message, data)
  if message == 'mouse_down' then
    local position = data
    local worldPoint = self.camera:screenToWorld(position)

    -- Pass this on to other interested game objects
    Notifier:postMessage('world_down', worldPoint)
  elseif message == 'agent_selected' then
    local agent = data
    self.agentInfoFrame:showForAgent(agent)
    self.agent = agent

  elseif message == 'agent_spawned' then
    local agent = data
    self.agentInfoFrame:showForAgent(agent)
    self.agent = agent

  elseif message == 'set_view_mode' then
    local mode = data
    self.viewMode = mode
  end
end

function scene:update(dt)
  Console:update(dt)
  Timer.update(dt)

  if self.paused then
    dt = 0
  end

  dt = dt * self.timescale

  self.gesturerecognizer:update(dt)
  loveframes.update(dt)

  -- Accumulate tick time
  self.tickDuration = self.tickDuration + dt
  if self.tickDuration > self.tickPeriod then
    -- Process ticks
    Notifier:postMessage('sim_tick')
    self.tickDuration = self.tickDuration - self.tickPeriod
  end

  Notifier:postMessage('ui_update', dt)

  -- Handle mouse panning
  local mouse = vector(love.mouse.getX(), love.mouse.getY())
  local cam_movement = vector(0, 0)
  if mouse.x <= 0 or love.keyboard.isDown('left') then
    cam_movement = cam_movement + vector(-1, 0)
  end
  if mouse.x >= love.graphics.getWidth() - 1 or love.keyboard.isDown('right') then
    cam_movement = cam_movement + vector(1, 0)
  end
  if mouse.y <= 0 or love.keyboard.isDown('up') then
    cam_movement = cam_movement + vector(0, -1)
  end
  if mouse.y >= love.graphics.getHeight() - 1 or love.keyboard.isDown('down') then
    cam_movement = cam_movement + vector(0, 1)
  end
  -- self.camera:setMovement(cam_movement)
  self.camera.focus = self.agent:getPosition()
  self.camera:update(dt)

  self.background:setViewport(self.camera:worldViewport())

  self.station:update(dt)

  for index, thing in ipairs(self.updatables) do
    thing:update(dt)
  end
end

function scene:draw()
  love.graphics.clear()

  love.graphics.setCanvas(Canvases.buffer)
  Canvases.buffer:clear(255, 255, 255)
  love.graphics.setPixelEffect()

  if self.viewMode == 'console' then

    self.stationConsole:draw()

    loveframes.draw()

  elseif self.viewMode == 'video' then

    Sprites.main.batch:clear()

    self.camera:apply()

    for index, entity in ipairs(self.drawables) do
      entity:draw()
    end

    love.graphics.draw(Sprites.main.batch)

    self.camera:unapply()

    loveframes.draw()

  else
    loveframes.draw()

  end

  love.graphics.setCanvas()
  Shaders:set(Shaders.currentEffect)

  love.graphics.drawq(Canvases.buffer, Shaders.quad, 0, 0)
  love.graphics.setPixelEffect()

  -- Non-shaded UI here

  Console:draw()
end

function scene:quit()
  love.event.push('quit')
end

function scene:leave()
end

return scene