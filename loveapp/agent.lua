require 'middleclass'
require 'vector'
require 'states'
require 'notifier'
require 'utility'

Agent = class('Agent')

local id = 1

function Agent:initialize(station)
  self.id = id
  id = id + 1

  self.spritesheet = Sprites.main

  -- World
  self.station = station

  -- Agent person information
  self.name = NameList:randomName('male')

  -- Size and position
  self.position = vector(0, 0)
  self.rotation = 0
  self.scale = 0.5
  self.flip = 1
  self.offset = vector(0, 0)

  -- Animation
  self.currentFrameIndex = 1
  self.animationName = 'agent_idle'
  self.animationElapsed = 0

  -- Needs
  self.needs = {
    fatigue = math.random(10, 100),
    hunger = math.random(10, 100),
    bathroom = math.random(10, 100),
    boredom = math.random(10, 100),
  }

  self.needs = {
    fatigue = 50,
    hunger = 50,
    bathroom = 50,
    boredom = 80,
  }

  self.happiness = 50
  self.health = 20

  -- Actions
  self.actions = {}

  Notifier:listenForMessage('world_down', self)
  Notifier:listenForMessage('sim_tick', self)

  self.state = States:getState('idle')
  self:setState(States:getState('select_action'))
end

function Agent:receiveMessage(message, data)
  if message == 'world_down' then
    local position = data
    if self:containsPoint(position) then
      Notifier:postMessage('agent_selected', self)
    end
  elseif message == 'sim_tick' then
    self:tick()
  end
end

function Agent:containsPoint(point)
  local size = self:getCurrentSize()
  return point.x >= self.position.x and
         point.x <= self.position.x + size.x and
         point.y >= self.position.y and
         point.y <= self.position.y + size.y
end

function Agent:getName()
  return self.name
end

function Agent:changeNeed(name, amount)
  self.needs[name] = math.clamp(self.needs[name] + amount, 0, 100)
end

function Agent:getNeedNames()
  return {
    'fatigue',
    'hunger',
    'bathroom',
    'boredom',
  }
end

function Agent:getHappiness()
  return self.happiness
end

function Agent:changeHappiness(amount)
  self.happiness = math.clamp(self.happiness + amount, 0, 100)
end

function Agent:getHealth()
  return self.health
end

function Agent:changeHealth(amount)
  self.health = math.clamp(self.health + amount, 0, 100)
end


function Agent:getNeed(name)
  return self.needs[name]
end

function Agent:scoreForRewards(rewards)
  -- score = ∑ all needs (A need (current value need) - A need (future value need))
  local score = 0

  print('checking rewards:')
  for reward, amount in pairs(rewards) do
    print(reward..' gives '..amount)
  end
  for index, needName in ipairs(self:getNeedNames()) do
    local future = self.needs[needName]
    if rewards[needName] then
      future = math.clamp(future + rewards[needName], -100, 100)
    end
    print(self.needs[needName]..' -> '..future..' for '..needName)

    score = score + 10 / self.needs[needName] - 10 / future
  end

  print('Total: '..score)
  if score < 0 then
  end

  return score
end

function Agent:setPosition(pos)
  self.position = pos
end

function Agent:getPosition()
  return self.position
end

function Agent:addAction(action)
  action:setAgent(self)
  action:setStation(self.station)

  table.insert(self.actions, action)
end

function Agent:setState(state)
  if (self.state ~= state) then
    self.state:exit()
    self.state = state
    self.state:enter(self)
  end
end

function Agent:setAnimation(name)
  if (self.animationName ~= name) then
    self.animationName = name
    self.currentFrameIndex = 1
    self.animationElapsed = 0
  end
end


function Agent:getCurrentSize()
  local animation = self.spritesheet.animations[self.animationName]
  local currentFrame = animation.frames[self.currentFrameIndex]
  local quad = self.spritesheet.quads[currentFrame.name]
  local x, y, w, h = quad:getViewport()
  return vector(w, h)
end

function Agent:update(dt)
  self.state:update(dt)

  -- Handle animation
  self.animationElapsed = self.animationElapsed + dt
  local animation = self.spritesheet.animations[self.animationName]
  if #animation.frames > 1 then -- More than one frame
    local duration = animation.frames[self.currentFrameIndex].duration

    if self.animationElapsed > duration then -- Switch to next frame
      self.currentFrameIndex = self.currentFrameIndex + 1
      if self.currentFrameIndex > #animation.frames then -- Aaaand back around
        self.currentFrameIndex = 1
      end
      self.animationElapsed = self.animationElapsed - duration
    end
  end
end

function Agent:tick()
    -- fatigue
    self:changeNeed('fatigue', -1)

    -- hunger
    self:changeNeed('hunger', -3)

    -- bathroom
    self:changeNeed('bathroom', -0.2)

    -- boredom
    self:changeNeed('boredom', -0.1)

    -- happiness
    for index, needName in ipairs(self:getNeedNames()) do
      if self.needs[needName] < 5 then
        self:changeHappiness(-4)
      elseif self.needs[needName] < 50 then
        self:changeHappiness(-2)
      elseif self.needs[needName] > 70 then
        self:changeHappiness(1)
      end
    end

    -- health
    -- Health regenerates at a rate of 1 point per tick
    self:changeHealth(1)
    if self.needs['hunger'] < 5 then
      self:changeHealth(-2)
    elseif self.needs['hunger'] < 20 then
      self:changeHealth(-1)
    end

    if self.needs['fatigue'] < 5 then
      self:changeHealth(-2)
    elseif self.needs['fatigue'] < 20 then
      self:changeHealth(-1)
    end
end

function Agent:draw()
  colors.white:set()

  local animation = self.spritesheet.animations[self.animationName]
  local currentFrame = animation.frames[self.currentFrameIndex]
  self.spritesheet.batch:addq(self.spritesheet.quads[currentFrame.name],
                              self.position.x,
                              self.position.y,
                              animation.rotation - self.rotation,
                              self.scale * self.flip,
                              self.scale,
                              self.offset.x,
                              self.offset.y)
end