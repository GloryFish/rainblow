local upgrades = {
  powercap1 = {
    id = 'powercap1',
    name = 'Battery Capacity Level 1',
    description = 'Increases station power capacity to 500',
    enable = function(station)
      station:setPowerCap(500)
    end,
    requirements = {},
    enabled = false,
  },
  powercap2 = {
    id = 'powercap2',
    name = 'Battery Capacity Level 2',
    description = 'Increases station power capacity to 1000',
    enable = function(station)
      station:setPowerCap(1000)
    end,
    requirements = {
      'powercap1',
    },
    enabled = false,
  },
  powercap3 = {
    id = 'powercap1',
    name = 'Battery Capacity Level 3',
    description = 'Increases station power capacity to 5000',
    enable = function(station)
      station:setPowerCap(5000)
    end,
    requirements = {
      'powercap1',
      'powercap2',
    },
    enabled = false,
  },
}

return upgrades

