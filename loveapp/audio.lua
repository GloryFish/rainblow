--
--  audio.lua
--

require 'middleclass'
require 'circle'
require 'vector'
require 'colors'
require 'notifier'
require 'label'

local Audio = class('Audio')

local groupLastPlayed = {}
local groupDelay = 2

function Audio:initialize()
  self.sounds = {}
  self.sound_ids = {}

  local soundlist = love.filesystem.enumerate('resources/sounds')
  local count = 0
  for i, filename in ipairs(soundlist) do
    local ext = filename:sub(-3)
    if ext == 'wav' or ext == 'mp3' then
      local source = love.audio.newSource('resources/sounds/' .. filename, 'static')
      self.sounds[filename] = source
      table.insert(self.sound_ids, filename)
      count = count + 1
    end
  end

  print('Loaded '..count..' sounds.')
end

function Audio:playSound(sound_id, group)
  if group == nil or groupLastPlayed[group] == nil or love.timer.getTime() - groupLastPlayed[group] > groupDelay then
    love.audio.play(self.sounds[sound_id])
    if group ~= nil then
      groupLastPlayed[group] = love.timer.getTime()
    end
  end
end

function Audio:playRandomSound(sound_ids, group)
  local sound_id = sound_ids[math.random(#sound_ids)]
  if group == nil or groupLastPlayed[group] == nil or (love.timer.getTime() - groupLastPlayed[group]) > groupDelay then
    love.audio.play(self.sounds[sound_id])
    if group ~= nil then
      groupLastPlayed[group] = love.timer.getTime()
    end
  end
end

function Audio:stopSound(sound_id)
  self.sounds[sound_id]:stop()
end

function Audio:update(dt)
end

return Audio()