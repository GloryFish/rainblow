--
--  bar.lua
--

require 'middleclass'
require 'vector'
require 'colors'

Bar = class('bar')

function Bar:initialize(attachedTo, color)
  self.width = 40
  self.height = 2
  self.amount = 1
  self.position = attachedTo.position
  self.offset = vector(0, -60)

  self.attachedTo = attachedTo
  self.color = color
end

function Bar:update(dt)
  self.position = self.attachedTo.position + self.offset
end

function Bar:draw()
  self.color:set()
    love.graphics.rectangle('fill', (self.position.x - self.width / 2), self.position.y, self.width * self.amount, self.height)

  colors.white:set()
  love.graphics.rectangle('line', self.position.x - self.width / 2 - 2, self.position.y - 2, self.width + 4, self.height + 4)
end