--
--  base.lua
--  rogue-descent
--
--  A base scene that can be used when creating new scenes.
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'tire'

local scene = Gamestate.new()

function scene:enter(pre)

  self.camera = Camera()
  self.camera.smoothMovement = true
  self.camera.deadzone = 0
  self.camera.useBounds = false
  self.camera.position = vector(0, 0)
  self.camera.focus = vector(0, 0)

  love.physics.setMeter(64)
  self.world = love.physics.newWorld(0, 0, true)
  self.tire = Tire(self.world)

end

function scene:keypressed(key, unicode)
  if key == 'escape' then
    self:quit()
  end
end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
end

function scene:update(dt)
  if love.mouse.isDown('l') then
  end

  self.world:update(dt)

  self.camera:update(dt)


  self.tire:update(dt)
end

function scene:draw()
  self.camera:apply()

  self.tire:draw()

  self.camera:unapply()
end

function scene:quit()
end

function scene:leave()
end

return scene