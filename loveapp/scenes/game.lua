--
--  base.lua
--  rogue-descent
--
--  A base scene that can be used when creating new scenes.
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'vector'
require 'colors'
require 'rectangle'
require 'world'
require 'rainbow'
require 'zombie'
require 'blood'
require 'cooldowntimer'
require 'scoreboard'

local scene = Gamestate.new()

function scene:enter(pre)
  Console.delegate = self

  self.gesturerecognizer = GestureRecognizer()

  self.paused = false
  self.timescale = 1

  self.path = self.world.paths[1]
  self.pathIndex = 1
  self.camera.focus = self.path[self.pathIndex]

  self.rainbow = Rainbow()

  -- Bounds for zombies
  self.zombounds = self.world:getBounds()

  self.camera.smoothMovement = false

  self.camera.deadzone = 0
  self.camera.smoothMovement = false
  self.camera.useBounds = true
  local min = self.zombounds:getMin()
  local max = self.zombounds:getMax()
  self.camera.bounds = {
    top = min.y - 150,
    left = min.x - 150,
    bottom = max.y + 150,
    right = max.x + 150,
  }

  self.maxZombies = 40

  self.blood = Blood()

  self.zombies = {}
  for i = 0, self.maxZombies do
    self:spawnZombie()
  end

  self.bossSpawned = false

  self.cooldowntimer = CooldownTimer(self.rainbow)
  self.cooldowntimer.scale = 0
  Tween.start(2, self.cooldowntimer, { scale = 1 }, 'outElastic')

  self.scoreboard = Scoreboard()

  Notifier:listenForMessage('mouse_moved', self)
  Notifier:listenForMessage('mouse_down', self)
  Notifier:listenForMessage('mouse_up', self)
  Notifier:listenForMessage('mouse_down_right', self)
  Notifier:listenForMessage('mouse_click', self)
  Notifier:listenForMessage('world_down', self)
  Notifier:listenForMessage('world_up', self)
  Notifier:listenForMessage('port_selected', self)
  Notifier:listenForMessage('zombie_died', self)
  Notifier:listenForMessage('needed_kills_reached', self)
  Notifier:listenForMessage('boss_zombie_died', self)
end

function scene:receiveMessage(message, data)
  if message == 'mouse_down' then
    local position = data
    local worldPoint = self.camera:screenToWorld(position)

    -- Pass this on to other interested game objects
    Notifier:postMessage('world_down', worldPoint)

  elseif message == 'mouse_up' then
    local position = data
    local worldPoint = self.camera:screenToWorld(position)

    -- Pass this on to other interested game objects
    Notifier:postMessage('world_up', worldPoint)

  elseif message == 'mouse_moved' then
    local position = data
    local worldPoint = self.camera:screenToWorld(position)

    -- Pass this on to other interested game objects
    Notifier:postMessage('world_moved', worldPoint)

  elseif message == 'world_down' then
    local worldPoint = data
    self.rainbow:startFiring(worldPoint)
    if self.rainbow.firing then
      self.camera.focus = worldPoint
      self.camera.position = worldPoint
      self.camera:startShaking()
      self.camera.speed = 0.5
      self.camera.scale = 2
      self.musichappy:setVolume(1)
      self.musicdark:setVolume(0.2)
    end
  elseif message == 'world_up' then
    if self.rainbow.firing then
      self.rainbow:stopFiring()
      self.camera:stopShaking()
      self.camera.speed = 0.8
      self.camera.scale = 1
      self.musichappy:setVolume(0)
      self.musicdark:setVolume(1)
    end
  elseif message == 'zombie_died' then
    local deadZombie = data

    self.blood:addGib(deadZombie.position)
    self.blood:addGib(deadZombie.position)
    self.blood:addGib(deadZombie.position)
    self.blood:addGib(deadZombie.position)

    for index, zombie in ipairs(self.zombies) do
      if zombie == deadZombie then
        table.remove(self.zombies, index)
        return
      end
    end

  elseif message == 'needed_kills_reached' then
    self.bossSpawned = true
    self:spawnZombieBoss()

  elseif message == 'boss_zombie_died' then
    self:win()
  end
end

function scene:keypressed(key, unicode)
  if Console:keypressed(key, unicode) then
    return
  end

  if key == 'escape' then
    self:quit()
  end

  if key == 'p' then
    self.paused = not self.paused
  end

  if key == ']' then
    self.timescale = self.timescale + 0.5
  end

  if key == '[' then
    self.timescale = self.timescale - 0.5
  end

  if key == '`' then
    Console:open()
  end

  if key == 'f' then
    System:toggleFullscreen()
  end
end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
end

function scene:update(dt)
  Timer.update(dt)
  Tween.update(dt)

  self.gesturerecognizer:update(dt)

  -- Camera panning
  local screenEdge = 100
  local mouse = vector(love.mouse.getX(), love.mouse.getY())
  local cam_movement = vector(0, 0)
  if mouse.x <= screenEdge then
    cam_movement = cam_movement + vector(-1, 0)
  end
  if mouse.x >= love.graphics.getWidth() - screenEdge then
    cam_movement = cam_movement + vector(1, 0)
  end
  if mouse.y <= screenEdge then
    cam_movement = cam_movement + vector(0, -1)
  end
  if mouse.y >= love.graphics.getHeight() - screenEdge then
    cam_movement = cam_movement + vector(0, 1)
  end
  self.camera:setMovement(cam_movement)
  self.camera:update(dt)

  -- Camera path following
  -- if self.camera.position:dist(self.path[self.pathIndex]) < 50 then
  --   print('Moving to: ' .. tostring(self.path[self.pathIndex]))
  --   self.pathIndex = self.pathIndex + 1
  --   if self.pathIndex > #self.path then
  --     self.pathIndex = 1
  --   end
  -- end

  self.rainbow:setTarget(self.camera:screenToWorld(vector(love.mouse.getX(), love.mouse.getY())))
  self.rainbow:update(dt)

  self.cooldowntimer:update(dt)

  self.scoreboard:update(dt)

  if #self.zombies < self.maxZombies then
    self:spawnZombie()
  end

  for index, zombie in ipairs(self.zombies) do
    zombie:update(dt, self.zombies, self.z)

    -- local max = self.zombounds:getMax()
    -- local min = self.zombounds:getMin()

    -- -- Wrap around
    -- if zombie.position.x < -zombie.size then
    --     zombie.position.x = max.x + zombie.size
    -- end
    -- if zombie.position.y < -zombie.size then
    --     zombie.position.y = max.y + zombie.size
    -- end
    -- if zombie.position.x > max.x + zombie.size then
    --     zombie.position.x = min.x - zombie.size
    -- end
    -- if zombie.position.y > max.y + zombie.size then
    --     zombie.position.y = min.y - zombie.size
    -- end
  end

  table.sort(self.zombies, function(a, b) return a.position.y < b.position.y end)
end

function scene:draw()
  love.graphics.clear()
  colors.white:set()

  love.graphics.setCanvas(Canvases.buffer)
  Canvases.buffer:clear(colors.green:unpack())
  love.graphics.setPixelEffect()

  Sprites.main.batch:clear()

  self.camera:apply()

  self.world:draw()

  colors.white:set()
  love.graphics.draw(Sprites.main.batch)

  self.blood:draw()

  colors.white:set()
  Sprites.zombies.batch:clear()
  for index, zombie in ipairs(self.zombies) do
    zombie:draw()
  end
  love.graphics.draw(Sprites.zombies.batch)

  self.rainbow:draw()

  colors.red:set()

  self.camera:unapply()

  colors.white:set()
  loveframes.draw()

  self.cooldowntimer:draw()
  self.scoreboard:draw()

  Sprites.ui.batch:clear()
  Cursor:draw()
  love.graphics.draw(Sprites.ui.batch)

  love.graphics.setCanvas()
  Shaders:set(Shaders.currentEffect)

  love.graphics.drawq(Canvases.buffer, Shaders.quad, 0, 0)
  love.graphics.setPixelEffect()

  Console:draw()

  love.graphics.setFont(fonts.small)
  love.graphics.print(string.format('Rainblow by Jay Roberts (build %s)', System.release), love.graphics.getWidth() - 430, 10)
end

function scene:spawnZombie()
  local zombie = Zombie(self.world)
  zombie:setPosition(self.world:randomSpawn())
  zombie:setBoundingRectangle(self.zombounds)
  zombie:setBloodEmitter(self.blood)

  table.insert(self.zombies, zombie)
end

function scene:spawnZombieBoss()
  local zombie = Zombie(self.world)
  zombie:setPosition(self.world:randomSpawn())
  zombie:setBoundingRectangle(self.zombounds)
  zombie:setBloodEmitter(self.blood)
  zombie:setBoss()

  table.insert(self.zombies, zombie)
end

function scene:win()
  Notifier:postMessage('world_up')

  scenes.victory.camera = self.camera
  scenes.victory.world = self.world
  Gamestate.switch(scenes.victory)
end

function scene:quit()
  Gamestate.switch(scenes.title)
end

function scene:leave()
end

return scene