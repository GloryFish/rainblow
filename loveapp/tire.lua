require 'middleclass'
require 'vector'
require 'colors'

Tire = class('Tire')

function Tire:initialize(world)
  self.body = love.physics.newBody(world, 0.25, 0.75, 'dynamic')
  self.shape = love.physics.newRectangleShape(0.5, 1.5)
  love.physics.newFixture(self.body, self.shape, 1)
end

function Tire:update(dt)
end

function Tire:draw()
  colors.black:set()
  love.graphics.circle('fill', 0, 0, 10, 10)

  colors.red:set()
  love.graphics.polygon('fill', self.body:getWorldPoints(self.shape:getPoints()))

  print(tostring(self.shape:getPoints()))
end