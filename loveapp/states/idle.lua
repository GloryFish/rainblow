require 'middleclass'

local StateIdle = class('StateIdle')

function StateIdle:initalize()
end

function StateIdle:enter()
end

function StateIdle:update(dt)
end

function StateIdle:exit()
end

return StateIdle
