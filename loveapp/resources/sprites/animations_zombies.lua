local animations = {
  zombie_1_right = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_1_right_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_1_right_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_1_right_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_1_right_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_1_left = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_1_left_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_1_left_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_1_left_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_1_left_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_1_up = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_1_up_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_1_up_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_1_up_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_1_up_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_1_down = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_1_down_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_1_down_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_1_down_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_1_down_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_2_right = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_2_right_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_2_right_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_2_right_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_2_right_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_2_left = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_2_left_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_2_left_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_2_left_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_2_left_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_2_up = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_2_up_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_2_up_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_2_up_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_2_up_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_2_down = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_2_down_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_2_down_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_2_down_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_2_down_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_3_right = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_3_right_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_3_right_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_3_right_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_3_right_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_3_left = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_3_left_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_3_left_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_3_left_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_3_left_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_3_up = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_3_up_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_3_up_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_3_up_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_3_up_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_3_down = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_3_down_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_3_down_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_3_down_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_3_down_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_4_right = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_4_right_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_4_right_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_4_right_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_4_right_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_4_left = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_4_left_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_4_left_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_4_left_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_4_left_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_4_up = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_4_up_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_4_up_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_4_up_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_4_up_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_4_down = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_4_down_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_4_down_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_4_down_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_4_down_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_5_right = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_5_right_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_5_right_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_5_right_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_5_right_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_5_left = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_5_left_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_5_left_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_5_left_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_5_left_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_5_up = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_5_up_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_5_up_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_5_up_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_5_up_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_5_down = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_5_down_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_5_down_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_5_down_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_5_down_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_6_right = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_6_right_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_6_right_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_6_right_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_6_right_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_6_left = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_6_left_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_6_left_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_6_left_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_6_left_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_6_up = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_6_up_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_6_up_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_6_up_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_6_up_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_6_down = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_6_down_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_6_down_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_6_down_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_6_down_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_7_right = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_7_right_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_7_right_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_7_right_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_7_right_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_7_left = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_7_left_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_7_left_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_7_left_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_7_left_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_7_up = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_7_up_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_7_up_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_7_up_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_7_up_2.png',
        duration = 0.3,
      },
    },
  },
  zombie_7_down = {
    rotation = 0,
    frames = {
      {
        name = 'zombie_7_down_1.png',
        duration = 0.3,
      },
      {
        name = 'zombie_7_down_2.png',
        duration = 0.3,
      },
      {
        name = 'zombie_7_down_3.png',
        duration = 0.3,
      },
      {
        name = 'zombie_7_down_2.png',
        duration = 0.3,
      },
    },
  },
}

return animations