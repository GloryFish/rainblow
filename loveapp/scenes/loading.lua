--
--  loading.lua
--  rogue-descent
--
--  Created by Jay Roberts on 2012-04-26.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'shaders'

local scene = Gamestate.new()

function scene:enter(pre)
  love.graphics.setBackgroundColor(255, 255, 255)

  self.progress = 0

  self.load = coroutine.create(function()
    local startTime = os.time()
    local progress = 0

    coroutine.yield()

    -- Set up vars
    vars = {
      debug = false,
      showhud = true,
      showgraph = false,
      showhash  = false,
      showwalls = false,
      showpatrols = false,
      showstats = false,
      showai = false;
      editor = false,
      sound = true,
      lognotifications = false,
    }

    -- Prepare system
    System = require 'system'

    progress = progress + 0.05
    coroutine.yield(progress)


    -- Prepare fonts
    fonts = {
      default        = love.graphics.newFont('resources/fonts/NotCourierSans.ttf', 20),
      small        = love.graphics.newFont('resources/fonts/NotCourierSans.ttf', 14),
    }

    progress = progress + 0.05
    coroutine.yield(progress)

    -- Prepare canvases
    Canvases = {
      buffer = love.graphics.newCanvas(),
    }

    progress = progress + 0.05
    coroutine.yield(progress)

    -- Prepare spritesheet
    Sprites = require 'spritesheets'

    progress = progress + 0.05
    coroutine.yield(progress)

    -- Prepare Advanced Tilemap Loader
    ATL = require 'atl'
    progress = progress + 0.05
    coroutine.yield(progress)

    -- Create console
    Console = require 'console'

    progress = progress + 0.05
    coroutine.yield(progress)

    -- Create stats
    Stats = require 'stats'

    progress = progress + 0.05
    coroutine.yield(progress)

    -- Load effects
    Shaders = require 'shaders'

    progress = progress + 0.05
    coroutine.yield(progress)

    -- Load timer library
    Timer = require 'timer'

    progress = progress + 0.05
    coroutine.yield(progress)

    -- Load tweening library
    Tween = require 'tween'

    progress = progress + 0.05
    coroutine.yield(progress)

    -- Load states
    States = require 'states'

    progress = progress + 0.05
    coroutine.yield(progress)

    -- Load cursor
    Cursor = require 'cursor'

    progress = progress + 0.05
    coroutine.yield(progress)

    Audio = require 'audio'

    progress = progress + 0.05
    coroutine.yield(progress)

    local totalTime = os.time() - startTime
    print('Loaded in '..tostring(totalTime)..' seconds')

    return 1
  end)
end

function scene:keypressed(key, unicode)
end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
end

function scene:update(dt)
  local result, progress = coroutine.resume(self.load)
  self.progress = progress or 1

  if coroutine.status(self.load) == 'dead' then
    Gamestate.switch(scenes.carphysics)
  end
end

function scene:draw()
  colors.black:set()
  love.graphics.print(string.format('Loading...%i%%', self.progress * 100), 30, 30)
end

function scene:quit()
end

function scene:leave()
end

return scene