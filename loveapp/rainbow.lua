require 'middleclass'
require 'vector'

Rainbow = class('Rainbow')

function Rainbow:initialize()
  self.beamSound = love.audio.newSource('resources/sounds/beam.mp3')
  self.beamSound:setLooping(true)
  self.beamSound:setVolume(0.5)
  self.beamSound:play()
  self.beamSound:pause()

  self.beam = {
    sprite = love.graphics.newImage('resources/sprites/rainbow_beam.png'),
  }
  self.beam.origin = vector(190, 480)

  self.highlight = {
    sprite = love.graphics.newImage('resources/sprites/rainbow_highlight.png'),
  }
  self.highlight.origin = vector(256, 256)
  self.highlight.alpha = 0
  self.highlight.currentTween = nil

  self.firing = false
  self.position = vector(0, 0)
  self.target = vector(0, 0)
  self.speed = 0.5

  self.blastDamage = 10
  self.blastRadius = 20

  self.cooldownTime = 5
  self.cooldownStart = love.timer.getTime() - self.cooldownTime
end

function Rainbow:startFiring(target)
  if self:cooldownProgress() < 1 then
    return
  end

  self.firing = true
  self.position = target
  self.target = target
  self.beamSound:resume()

  Tween.stop(self.highlight.currentTween)
  self.highlight.currentTween = Tween.start(3, self.highlight, { alpha = 255 }, 'outQuad')
end

function Rainbow:setTarget(target)
  self.target = target
end

function Rainbow:cooldownProgress()
  local progress = (love.timer.getTime() - self.cooldownStart) / self.cooldownTime
  return math.min(progress, 1)
end

function Rainbow:stopFiring()
  self.cooldownStart = love.timer.getTime()
  self.firing = false
  Tween.stop(self.highlight.currentTween)
  self.highlight.currentTween = Tween.start(3, self.highlight, { alpha = 0 }, 'outQuad')
  self.beamSound:pause()
end

function Rainbow:update(dt)
  if self.firing then
    -- Position rainbow and particles
    local movement = self.target - self.position
    self.position = self.position + movement * dt * self.speed

    Notifier:postMessage('rainbow_blast', {
      position = self.position,
      radius = self.blastRadius,
      damage = self.blastDamage,
    })
  else
    Notifier:postMessage('rainbow_calm')
  end
end

function Rainbow:draw()
  if self.firing then
    -- Draw rainbow
    colors.white:set()
    love.graphics.draw(self.beam.sprite, self.position.x, self.position.y, 0, 1, 1, self.beam.origin.x, self.beam.origin.y)
  end
  colors.white:alpha(self.highlight.alpha):set()
  love.graphics.draw(self.highlight.sprite, self.position.x, self.position.y, 0, 1, 1, self.highlight.origin.x, self.highlight.origin.y)
end
