--
--  textbutton.lua
--  desert
--
--  Created by Jay Roberts on 2011-03-18.
--  Copyright 2011 GloryFish.org. All rights reserved.
--

require 'middleclass'
require 'vector'
require 'colors'

TextButton = class('TextButton')

function TextButton:initialize(text)
  self.position = vector(0, 0)
  self.text = text
  self.color = Color(176, 225, 252)
  self.font = love.graphics.newFont('resources/fonts/Herculanum.ttf', 30)
  self.fontSelected = love.graphics.newFont('resources/fonts/Herculanum.ttf', 36)

  self.width = self.font:getWidth(self.text)
  self.height = self.font:getHeight(self.text)
  self.selectedHeight = self.fontSelected:getHeight(self.text)

  self.selected = false

  self.action = nil

end

function TextButton:mousepressed(pos)
  self.mousePos = pos
  if self:containsPosition(pos) then
    self.selected = true
  end
end

function TextButton:mousereleased(pos)
  self.mousePos = pos
  self.selected = false

  if self:containsPosition(pos) then
    self:runAction()
  end
end

function TextButton:runAction()
  if self.action ~= nil then
    self:action()
  end
end

function TextButton:containsPosition(pos)
  local lineWidth = self.font:getWidth(self.text)
  local halfWidth = lineWidth / 2
  local halfHeight = self.height / 2

  if pos.x >= self.position.x - halfWidth and
     pos.x <= self.position.x + halfWidth and
     pos.y >= self.position.y - halfHeight and
     pos.y <= self.position.y + halfHeight then
    return true
  else
    return false
  end
end


function TextButton:draw()
  local prevFont = love.graphics.getFont()

  local font = self.font
  local height = self.height
  if self.selected then
    font = self.fontSelected
    height = self.selectedHeight
  end

  love.graphics.setFont(font)
  local lineWidth = font:getWidth(self.text)


  colors.black:set()
  love.graphics.print(self.text,
                      (self.position.x - lineWidth / 2) + 1,
                      self.position.y - height / 2 + 1)

  self.color:set()

  love.graphics.print(self.text,
                      self.position.x - lineWidth / 2,
                      self.position.y - height / 2)

  love.graphics.setFont(prevFont)
end