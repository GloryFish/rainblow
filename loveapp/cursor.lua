--
--  cursor.lua
--

require 'middleclass'
require 'vector'
require 'colors'

local Cursor = class('Cursor')

function Cursor:initialize()
  love.mouse.setVisible(false)
  self.spritesheet = Sprites.ui
  self.scale = 1
  self.offset = vector(0, 0)

  self.quad_name = 'cursor_pointer.png'
end

function Cursor:update(dt)
  if self.quad_name == 'cursor_pointer.png' then
    self.offset = vector(0, 0)
  elseif self.quad_name == 'cursor_x.png' then
    self.offset = vector(8, 8)
  end
end

function Cursor:set(name)
  self.quad_name = name
end

function Cursor:draw()
  colors.white:set()
  self.spritesheet.batch:addq(self.spritesheet.quads[self.quad_name],
                            love.mouse.getX(),
                            love.mouse.getY(),
                            0,
                            self.scale,
                            self.scale,
                            self.offset.x,
                            self.offset.y)
end

return Cursor()