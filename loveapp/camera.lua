--
--  camera.lua
--  redditgamejam-05
--
--  Created by Jay Roberts on 2011-01-06.
--  Copyright 2011 GloryFish.org. All rights reserved.
--

require 'middleclass'
require 'vector'
require 'noise'
require 'rectangle'

local cos, sin = math.cos, math.sin

Camera = class('Camera')

function Camera:initialize()
  self.bounds = {
    top = 0,
    right = love.graphics.getWidth(),
    bottom = love.graphics.getHeight(),
    left = 0
  }
  self.useBounds = false
  self.position = vector(love.graphics.getWidth() / 2, love.graphics.getHeight() / 2)
  self.offset = self.position
  self.focus = vector(love.graphics.getWidth() / 2, love.graphics.getHeight() / 2)
  self.deadzone = 0

  self.smoothMovement = true
  self.movement = vector(0, 0)

  self.handheld = true
  self.handheldAmount = vector(0, 0)
  self.handheldNoise = {
    horizontal = Noise:perlin(1000, 1),
    vertical   = Noise:perlin(1000, 2),
  }
  self.handheldMax = vector(17, 17)

  self.shakeDuration = 0
  self.shakeTime = 0
  self.shakeAmount = vector(0, 0)
  self.shakeMax = vector(20, 20)
  self.shakeIntensity = 1
  self.shaking = false


  self.speed = 0.8 -- 1 is normal speed
  self.manual_speed = 200

  self.scale = 1

  self.rotation = 0

  self.elapsed = 0
end

function Camera:setMovement(movement)
  self.movement = movement
end

function Camera:worldToScreen(position)
  local position = position:clone()

  local w, h = love.graphics.getWidth(), love.graphics.getHeight()
  local c, s = cos(self.rotation), sin(self.rotation)
  position.x = position.x - self.position.x
  position.y = position.y - self.position.y

  position.x = c * position.x - s * position.y
  position.y = s * position.x + c * position.y

  return vector(position.x * self.scale + w / 2, position.y * self.scale + h / 2)
end

function Camera:screenToWorld(position)
  local position = position:clone()

  local w, h = love.graphics.getWidth(), love.graphics.getHeight()
  local c, s = cos(-self.rotation), sin(-self.rotation)

  position.x = (position.x - w / 2) / self.scale
  position.y = (position.y - h / 2) / self.scale

  position.x = c * position.x - s * position.y
  position.y = s * position.x + c * position.y

  return vector(position.x + self.position.x, position.y + self.position.y)
end

function Camera:worldViewport()
  local upperLeft = self:screenToWorld(vector(0, 0))
  local lowerRight = self:screenToWorld(vector(love.graphics.getWidth(), love.graphics.getHeight()))
  return Rectangle(upperLeft, lowerRight - upperLeft)
end

function Camera:update(dt)
  self.elapsed = self.elapsed + dt

  if self.handheld then
    -- Sample horizontal and vertical perturbation amounts
    local index = math.ceil((0.1 * self.elapsed * 60) % 1000) -- 1000 samples at 10 samples per second gives us 100 seconds of unique movement

    self.handheldAmount = vector(math.floor(self.handheldNoise.horizontal[index] * self.handheldMax.x), math.floor(self.handheldNoise.vertical[index] * self.handheldMax.y))
  else
    self.handheldAmount = vector(0 , 0)
  end

  -- Move the camera if we are outside the deadzone
  -- local adjustedFocus = self.focus + self.handheldAmount
  -- if self.smoothMovement then
  --   if self.position:dist(adjustedFocus) > self.deadzone then
  --     self.position = self.position - (self.position - adjustedFocus) * dt * self.speed
  --   end
  -- else
  --   self.position = adjustedFocus
  -- end

  -- Apply manual camera movement
  self.position = self.position + (self.movement * self.manual_speed * dt)

  -- Clamp camera to bounds
  if self.useBounds then
    local upperLeft = self:screenToWorld(vector(0, 0))
    local lowerRight = self:screenToWorld(vector(love.graphics.getWidth(), love.graphics.getHeight()))

    local halfWidth = (lowerRight.x - upperLeft.x) / 2
    local halfHeight = (lowerRight.y - upperLeft.y) / 2

    if upperLeft.x < self.bounds.left then
      self.position.x = self.bounds.left + halfWidth
    end

    if self.position.x + halfWidth > self.bounds.right then
      self.position.x = self.bounds.right - halfWidth
    end

    if self.position.y - halfHeight < self.bounds.top then
      self.position.y = self.bounds.top + halfHeight
    end

    if self.position.y + halfHeight > self.bounds.bottom then
      self.position.y = self.bounds.bottom - halfHeight
    end
  end

  -- Update the offset
  self.offset = vector(math.floor(self.position.x - love.graphics.getWidth() / 2),
                       math.floor(self.position.y - love.graphics.getHeight() / 2))

  if self.shaking or self.shakeDuration > 0 then
    self.shakeAmount = vector(math.random() * self.shakeMax.x, math.random() * self.shakeMax.y)

    self.shakeDuration = self.shakeDuration - dt
  end

end

function Camera:startShaking(intensity)
  self.shaking = true
  self.shakeIntensity = intensity or 1
end

function Camera:stopShaking()
  self.shaking = false
end

function Camera:shake(duration, intesity)
  self.shakeDuration = duration
  self.shakeTime = duration
  self.shakeIntensity = intesity
  if self.shakeIntensity > 1 then
    self.shakeIntensity = 1
  end
end


function Camera:apply()
  local cx, cy = love.graphics.getWidth() / (2 * self.scale), love.graphics.getHeight() / (2 * self.scale)
  local camPos =  self.position + self.shakeAmount

  love.graphics.push()
  love.graphics.scale(self.scale)
  love.graphics.translate(cx, cy)
  love.graphics.rotate(self.rotation)
  love.graphics.translate(math.floor(-camPos.x), math.floor(-camPos.y))
end

function Camera:unapply()
  love.graphics.pop()
end