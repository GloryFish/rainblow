--
--  system.lua
--

require 'middleclass'

local System = class('System')

function System:initialize()
  self.release = 'unspecified'
  if love.filesystem.isFile('release.txt') then
    local releaseinfo = love.filesystem.read('release.txt')
    self.release = releaseinfo:gsub('^%s*(.-)%s*$', '%1')
  end

  self.fullscreen = false

  local modes = love.graphics.getModes()
  table.sort(modes, function(a, b) return a.width*a.height < b.width*b.height end)
  local largestMode = modes[#modes]

  -- Define the desired, native, fullscreen, and windowed resolutions of the game
  self.desiredRes = {
    x = 600, -- fullscreen will be horiz+ so the actual native width may be different
    y = 600,
  }

  self.nativeRes = {
    x = 600,
    y = 600,
  }

  self.fullscreenRes = {
    x = largestMode.width,
    y = largestMode.height,
  }

  self.windowedRes = {
    x = 600,
    y = 600,
  }

  self.scalingFactor = {
    x = love.graphics.getWidth() / self.nativeRes.x,
    y = love.graphics.getHeight() / self.nativeRes.y,
  }

  -- Wrap love mouse functions so we can transform window coordinates to
  -- native game resolution coordinates.
  self.nativeMousePos = love.mouse.getPosition
  love.mouse.getPosition = function()
    local x, y = self:nativeMousePos()
    return x / self.scalingFactor.x, y / self.scalingFactor.y
  end

  self.nativeMousePosX = love.mouse.getX
  love.mouse.getX = function()
    local x = self.nativeMousePosX()
    return x / self.scalingFactor.x
  end

  self.nativeMousePosY = love.mouse.getY
  love.mouse.getY = function()
    local y = self.nativeMousePosY()
    return y / self.scalingFactor.y
  end

  self.nativeMousePressed = love.mousepressed
  love.mousepressed = function(x, y, button)
    local xratio = love.graphics.getWidth() / self.nativeRes.x
    local yratio = love.graphics.getHeight() / self.nativeRes.y

    self.nativeMousePressed(x / self.scalingFactor.x, y / self.scalingFactor.y, button)
  end

  self.nativeMouseReleased = love.mousereleased
  love.mousereleased = function(x, y, button)
    self.nativeMouseReleased(x / self.scalingFactor.x, y / self.scalingFactor.y, button)
  end
end


function System:setFullscreenResolution(width, height)
  self.fullscreenRes = {
    x = width,
    y = height,
  }
  self:reloadWindow()
end

function System:setWindowedResolution(width, height)
  self.windowedRes = {
    x = width,
    y = height,
  }
  self:reloadWindow()
end

function System:toggleFullscreen()
  -- self.fullscreen = not self.fullscreen
  -- self:reloadWindow()
  -- love.graphics.toggleFullscreen()
end

function System:reloadWindow()
  if self.fullscreen then
    local aspect = self.fullscreenRes.x / self.fullscreenRes.y
    self.nativeRes = {
      x = self.desiredRes.y * aspect,
      y = self.desiredRes.y
    }

    love.graphics.setMode(self.fullscreenRes.x, self.fullscreenRes.y, true, true, 0)
    Canvases = {
      buffer = love.graphics.newCanvas(self.nativeRes.x, self.nativeRes.y),
    }
    Canvases.buffer:setFilter('nearest', 'nearest')
  else
    local aspect = self.fullscreenRes.x / self.fullscreenRes.y
    self.nativeRes = self.desiredRes

    love.graphics.setMode(self.windowedRes.x, self.windowedRes.y, false, true, 0)
    Canvases = {
      buffer = love.graphics.newCanvas(self.nativeRes.x, self.nativeRes.y),
    }
    Canvases.buffer:setFilter('nearest', 'nearest')
  end

  self.scalingFactor = {
    x = love.graphics.getWidth() / self.nativeRes.x,
    y = love.graphics.getHeight() / self.nativeRes.y,
  }

  Notifier:postMessage('window_reload')
end

function System:getWidth()
  return self.nativeRes.x
end

function System:getHeight()
  return self.nativeRes.y
end

return System()