module(...)

-- This file is for use with Love2d and was generated by Zwoptex (http://zwoptexapp.com/)
--
-- The function getSpriteSheetData() returns a table suitable for importing using sprite.newSpriteSheetFromData()
--
-- Usage example:
--			local zwoptexData = require "ThisFile.lua"
-- 			local data = zwoptexData.getSpriteSheetData()
--			local spriteSheet = sprite.newSpriteSheetFromData( "Untitled.png", data )
--
-- For more details, see http://developer.anscamobile.com/content/game-edition-sprite-sheets

function getFrames()
	local frames = {
		
			{
				name = "cursor_pointer.png",
				rect = { x = 2, y = 2, width = 16, height = 16 }, 
			},
		
			{
				name = "cursor_x.png",
				rect = { x = 2, y = 20, width = 16, height = 16 }, 
			},
		
	}

	return frames
end