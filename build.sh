# Clean build directory
rm -rf build/*

# Export release info
git rev-parse --short HEAD > loveapp/release.txt

# Build .love
cd loveapp
zip -r ../build/rainblow.love *

cd ../build

# build Windows
cp -R ../love-windows ./rainblow-win
cat ./rainblow-win/love.exe ./rainblow.love > ./rainblow-win/rainblow.exe
rm ./rainblow-win/love.exe
# Build Mac

cp -R /Applications/love.app ../build/Rainblow.app
cp ./rainblow.love ./Rainblow.app/Contents/Resources/
/usr/libexec/PlistBuddy -c "Set :CFBundleIdentifier org.gloryfish.rainblow" ./Rainblow.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Set :CFBundleName Rainblow" ./Rainblow.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Set :CFBundleSignature org.gloryfish.rainblow" ./Rainblow.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Delete :UTExportedTypeDeclarations" ./Rainblow.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Delete :CFBundleDocumentTypes" ./Rainblow.app/Contents/Info.plist

cp ../icon.icns ./Rainblow.app/Contents/Resources/Love.icns